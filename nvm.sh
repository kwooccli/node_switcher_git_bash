#!/bin/bash

#init vars
OPTIND=1
setbashrc=false


#get options
while getopts ":i" opt; do
  case $opt in
    i)
      #echo "found argument i" >&2
      setbashrc=true
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      return
      ;;
  esac
done


#echo "num args $#";
# check for correct number of args
if [ "$setbashrc" = "true" ]; then 
    if [ $# -ne 2 ]; then 
        echo "bashrc: Please provide a valid node js version number $#" >&2
        return
    else
        ver=$2
    fi

else
    if [ $# -ne 1 ] ; then 
        echo "Please provide a valid node js version number $#" >&2
        return
    else
        ver=$1
    fi
fi


vername=node-v$ver-win-x64
weburl=https://nodejs.org/download/release/v$ver/$vername.zip
node_root_dir=/c/nodejs
node_ver_dir=$node_root_dir/$vername

#download node version if it doesn't already exists
if [ ! -d $node_ver_dir ]; then
    
    if  (curl -o/dev/null -sfI $weburl)
    then
        pushd /c/nodejs
        echo "Downloading from $weburl" >&2
        curl -o /c/nodejs/$vername.zip $weburl
        echo "Unzipping $vername" >&2
        unzip -q -o /c/nodejs/$vername.zip 
        echo "Done Unzipping" >&2
        rm -rf /c/nodejs/$vername.zip
        popd
    else
        echo "That version of node JS ($ver) does not exist. "  >&2
        echo "Please provide a valid node JS Version"  >&2
        return
    fi
fi

#update current path
#rm -rf /c/nodejs/main
if [ -z "$NODEJS_PATH" ]; then
    echo "NODEJS_PATH empty $node_ver_dir" >&2
    export PATH=$PATH:$node_ver_dir
else
    export PATH=$(echo "$PATH" | sed -e "s|:$NODEJS_PATH|:$node_ver_dir|g")
fi


export NODEJS_PATH=$node_ver_dir
echo "Updated to $ver successfully" >&2

#update ~/.bashrc file
if [ "$setbashrc" = "true" ]; then 
    echo "Setting ~/.bashrc file" >&2      
    
    if [ ! -f ~/.bashrc ]
    then
        echo "bashrc file doesn't exist... creating one" >&2      
        touch ~/.bashrc
    fi

    
    if [ `cat ~/.bashrc | grep "#NODEJS_ENV1" | wc -l` -ne 0 ]
    then
        sed -i "s|.*#NODEJS_ENV1.*|export NODEJS_PATH=$NODEJS_PATH #NODEJS_ENV1|g" ~/.bashrc
        echo "SED NODEJS_PATH" >&2      
    else
        echo "export NODEJS_PATH=$NODEJS_PATH #NODEJS_ENV1" >> ~/.bashrc
        echo "export NODEJS_PATH" >&2      
    fi

    if [ $(cat ~/.bashrc | grep "#NODEJS_ENV1$" | wc -l) -ne 0 ]
    then
        sed -i "s|.*#NODEJS_ENV2.*|export PATH=\$PATH:/c/nodejs/:$NODEJS_PATH #NODEJS_ENV2|g" ~/.bashrc
        echo "SED PATH" >&2      
    else
        echo "export PATH=\$PATH:/c/nodejs/:$NODEJS_PATH #NODEJS_ENV2" >> ~/.bashrc
        echo "export PATH" >&2      
    fi
fi





