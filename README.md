# node_switcher_git_bash

This is a shell tool used to switch between node versions on windows


## Setup

You must have git bash installed since this tool can only be used on gitbash

1. Checkout current repo into `c:\nodejs` where nodejs will contain the `nvm.sh` file
2. Run the script to set default value

## Running

```
source /c/nodejs/nvm.sh <version>
```

to set default value in future git bash opens

```
source /c/nodejs/nvm.sh -i <version>
```

## To do

1. arg to provide means to get latest vs stable
2. arg to list versions downloaded/installed
3. arg to list all versions available